# Tier Android Challenge

## Project Architecture

Clean architecture is the project the proper architecture. 
It is consists 3 different layers, 

![Clean architecture](https://koenig-media.raywenderlich.com/uploads/2019/06/Clean-Architecture-graph.png)

    - Data layer (Apis, network connection)
    - Domain layer (Business logic)
    - Presentation layer (UI and ViewModels)

## Android Specficiations

### Main Components

In this project, I focused to get the latest SDK, kotlin, versions

Target Android SDK **31** 

Kotlin version **1.6.20**

### Android Libraries

- [Dagger Hilt](https://developer.android.com/training/dependency-injection/hilt-android)
- [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel)
- [Coroutines](https://kotlinlang.org/docs/coroutines-guide.html)
- [MockK](https://mockk.io/)
- [ActivityPermsion](https://developer.android.com/training/permissions/requesting) : The latest version of requesting permissions
- [Google Maps](https://developers.google.com/maps/documentation/android-sdk/map-with-marker)
- [Clustering](https://developers.google.com/maps/documentation/android-sdk/utility/marker-clustering?hl=en)
- [Data Binding](https://developer.android.com/topic/libraries/data-binding)


## Code quality

### Unit testing

We used Mockk Library along with Mock library. 

### Lint Checker & Formater

We used [KtLint](https://ktlint.github.io) library for check lint issues and code formatter.

## Continuous Integration

We built out CI model from 2 stages. The first stage has 2 jobs, 
assembleDebug, and lintdebug (we used ktlint for the CI as well.). For the second stage, we run a single
job, DebugTest to run the test cases.

I followed my [own article on medium](https://proandroiddev.com/devops-understanding-and-applying-ci-cd-pipeline-for-android-developers-part-1-cdbeab424781) about CI/CD for Android using GitLab.


## Authors and acknowledgment
Thanks Tier company, letting me participate in the code challenge.

## License
For open source projects, say how it is licensed.
