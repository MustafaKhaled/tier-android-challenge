package com.khaled.tierandroidchallenge.util

import android.content.Context
import android.content.res.Resources
import android.view.View
import android.widget.ImageView
import io.mockk.clearAllMocks
import org.junit.After
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify

class BindingAdaptersTest {
    @Test
    fun test_view_visibility_by_passing_true_should_be_visible() {
        val view = mock(View::class.java)
        val visibility = true
        BindingAdapters.setVisibility(view, visibility)
        verify(view).visibility = View.VISIBLE
    }

    @Test
    fun test_view_visibility_by_passing_false_should_be_invisible() {
        val view = mock(View::class.java)
        val visibility = true
        BindingAdapters.setVisibility(view, visibility)
        verify(view).visibility = View.VISIBLE
    }

    @Test
    fun test_imageview_check_passed_image_id() {
        val imageView = mock(ImageView::class.java)
        val context = mock(Context::class.java)
        val resources = mock(Resources::class.java)
        val resourceName = "resource_name"
        val packageName = "package_name"
        val resourceId = 0x00000000

        Mockito.`when`(imageView.context).thenReturn(context)
        Mockito.`when`(context.resources).thenReturn(resources)
        Mockito.`when`(context.packageName).thenReturn(packageName)
        Mockito.`when`(
            resources.getIdentifier(
                resourceName,
                "drawable",
                packageName
            )
        ).thenReturn(resourceId)
        BindingAdapters.setImageViewSource(imageView, resourceId)
        verify(imageView).setImageResource(resourceId)
    }
    @After
    fun tearDown() {
        clearAllMocks()
    }
}
