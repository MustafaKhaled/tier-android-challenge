package com.khaled.tierandroidchallenge.data.impl.impl

import com.khaled.tierandroidchallenge.data.remote.EndPoints
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Test
@ExperimentalCoroutinesApi
class ScootersDataImplTest {
    private val endpoint = mockk<EndPoints>(relaxed = true)

    @Test
    fun verify_getNearbyScooters_from_endpoint_called() = runBlocking {
        val scootersDataImpl = ScootersDataImpl(endpoint)
        scootersDataImpl.getScooters()
        coVerify { endpoint.getNearbyScooters() }
    }
}
