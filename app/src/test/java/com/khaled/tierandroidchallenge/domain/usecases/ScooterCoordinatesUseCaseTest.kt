package com.khaled.tierandroidchallenge.domain.usecases

import com.khaled.tierandroidchallenge.common.Resource
import com.khaled.tierandroidchallenge.data.model.NearbyScooters
import com.khaled.tierandroidchallenge.data.model.Scooter
import com.khaled.tierandroidchallenge.domain.errorhandling.NetworkErrorHandler
import com.khaled.tierandroidchallenge.domain.repo.IScootersData
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Test
import java.lang.Exception

class ScooterCoordinatesUseCaseTest {
    @Test
    fun verify_getScooters_shouldReturnData_Successfully() = runBlocking {
        var result: Resource<NearbyScooters>? = null
        val scooters = ArrayList<Scooter>()
        val nearbyScooter = NearbyScooters(scooters)
        val iScooterData = mockk<IScootersData>(relaxed = true)
        val errorHandler = mockk<NetworkErrorHandler>(relaxed = true)
        val useCase = ScooterCoordinatesUseCase(iScooterData, errorHandler)
        coEvery { iScooterData.getScooters() } returns nearbyScooter
        useCase.getNearbyScooters().collect {
            result = it
        }
        assertEquals(Resource.Success(nearbyScooter), result)
    }

    @Test
    fun verify_getScooters_shouldReturnLoadingStateAndSuccessState() = runBlocking {
        val result = ArrayList<Resource<NearbyScooters>>()
        val scooters = ArrayList<Scooter>()
        val nearbyScooter = NearbyScooters(scooters)
        val iScooterData = mockk<IScootersData>(relaxed = true)
        val errorHandler = mockk<NetworkErrorHandler>(relaxed = true)
        val useCase = ScooterCoordinatesUseCase(iScooterData, errorHandler)
        coEvery { iScooterData.getScooters() } returns nearbyScooter
        useCase.getNearbyScooters().collect {
            result.add(it)
        }
        assertEquals(listOf(Resource.Loading, Resource.Success(nearbyScooter)), result)
    }

    @Test
    fun verify_getScooters_shouldReturnFailure() = runBlocking {
        val result = ArrayList<Resource<NearbyScooters>>()
        val exception = mockk<Exception>(relaxed = true)
        val iScooterData = mockk<IScootersData>(relaxed = true)
        val errorHandler = mockk<NetworkErrorHandler>(relaxed = true)
        val useCase = ScooterCoordinatesUseCase(iScooterData, errorHandler)
        coEvery { iScooterData.getScooters() } throws exception
        useCase.getNearbyScooters().collect {
            result.add(it)
        }
        assertEquals(listOf(Resource.Loading, Resource.Failure(errorHandler.getError(exception))), result)
    }
}
