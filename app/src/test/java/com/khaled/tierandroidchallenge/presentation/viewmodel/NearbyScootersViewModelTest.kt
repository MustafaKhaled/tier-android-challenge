package com.khaled.tierandroidchallenge.presentation.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.khaled.tierandroidchallenge.common.Resource
import com.khaled.tierandroidchallenge.data.model.NearbyScooters
import com.khaled.tierandroidchallenge.domain.usecases.ScooterCoordinatesUseCase
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

@ExperimentalCoroutinesApi
class NearbyScootersViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()
    private val testDispatcher = UnconfinedTestDispatcher()
    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)
    }
    @Test
    fun test_livedata_Loading_state_when_calling_network_request_function_return_success() = runBlocking {
        val useCase = mockk<ScooterCoordinatesUseCase>(relaxed = true)
        val expected = Resource.Loading
        val nearbyScootersViewModel = NearbyScootersViewModel(useCase)
        coEvery { useCase.getNearbyScooters() } returns flowOf(expected)
        nearbyScootersViewModel.event.observeForever {}
        nearbyScootersViewModel.getNearbyScooters()
        assertEquals(expected, nearbyScootersViewModel.event.value)
    }

    @Test
    fun test_livedata_success_state_when_calling_network_request_function_return_success() = runBlocking {
        val useCase = mockk<ScooterCoordinatesUseCase>(relaxed = true)
        val mockedObject = mockk<NearbyScooters>(relaxed = true)
        val expected = Resource.Success(mockedObject)
        val nearbyScootersViewModel = NearbyScootersViewModel(useCase)
        coEvery { useCase.getNearbyScooters() } returns flowOf(expected)
        nearbyScootersViewModel.event.observeForever {}
        nearbyScootersViewModel.getNearbyScooters()
        assertEquals(expected, nearbyScootersViewModel.event.value)
    }

    @Test
    fun test_livedata_failure_state_when_calling_network_request_function_return_success() = runBlocking {
        val useCase = mockk<ScooterCoordinatesUseCase>(relaxed = true)
        val error = "failed"
        val expected = Resource.Failure(error)
        val nearbyScootersViewModel = NearbyScootersViewModel(useCase)
        coEvery { useCase.getNearbyScooters() } returns flowOf(expected)
        nearbyScootersViewModel.event.observeForever {}
        nearbyScootersViewModel.getNearbyScooters()
        assertEquals(expected, nearbyScootersViewModel.event.value)
    }

    @Test
    fun test_showProgressBar_visible_when_passing_true_should_be_visible() = runBlocking {
        val useCase = mockk<ScooterCoordinatesUseCase>(relaxed = true)
        val nearbyScootersViewModel = NearbyScootersViewModel(useCase)
        nearbyScootersViewModel.showProgressBar(true)
        assertTrue(nearbyScootersViewModel.showProgressBar.get())
    }

    @Test
    fun test_showProgressBar_visible_when_passing_false_should_be_invisible() = runBlocking {
        val useCase = mockk<ScooterCoordinatesUseCase>(relaxed = true)
        val nearbyScootersViewModel = NearbyScootersViewModel(useCase)
        nearbyScootersViewModel.showProgressBar(false)
        assertFalse(nearbyScootersViewModel.showProgressBar.get())
    }

    @Test
    fun test_setVehicleData_should_equal_passed_more_than_zero() = runBlocking {
        val useCase = mockk<ScooterCoordinatesUseCase>(relaxed = true)
        val nearbyScootersViewModel = NearbyScootersViewModel(useCase)
        nearbyScootersViewModel.setVehiclesData(10)
        assertTrue(nearbyScootersViewModel.availableVehicle.get()> 0)
    }

    @Test
    fun test_setVehicleData_should_equal_passed_equal_zero() = runBlocking {
        val useCase = mockk<ScooterCoordinatesUseCase>(relaxed = true)
        val nearbyScootersViewModel = NearbyScootersViewModel(useCase)
        nearbyScootersViewModel.setVehiclesData(0)
        assertTrue(nearbyScootersViewModel.availableVehicle.get() == 0)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }
}
