package com.khaled.tierandroidchallenge.ui

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.khaled.tierandroidchallenge.common.TAG
import com.khaled.tierandroidchallenge.databinding.LayoutPermissionInfoDialogBinding

class PermissionInfoDialog : DialogFragment() {
    private var _binding: LayoutPermissionInfoDialogBinding? = null
    private val binding get() = _binding!!

    var onDialogListener: OnDialogListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            onDialogListener = activity as OnDialogListener?
        } catch (e: ClassCastException) {
            Log.e(TAG, "onAttach: " + e.message)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = LayoutPermissionInfoDialogBinding.inflate(inflater, container, false)
        handleDialogButtonsListener()
        return binding.root
    }

    private fun handleDialogButtonsListener() {
        binding.acceptBtn.setOnClickListener {
            onDialogListener?.onAcceptPressed()
        }
        binding.noThanksBtn.setOnClickListener {
            dismiss()
            onDialogListener?.onNoThanksPressed()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    interface OnDialogListener {
        fun onAcceptPressed()
        fun onNoThanksPressed()
    }
}
