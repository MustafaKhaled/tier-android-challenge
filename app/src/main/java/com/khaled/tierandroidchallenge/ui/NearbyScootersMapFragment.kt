package com.khaled.tierandroidchallenge.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.maps.android.clustering.ClusterManager
import com.khaled.tierandroidchallenge.BR
import com.khaled.tierandroidchallenge.R
import com.khaled.tierandroidchallenge.common.Resource
import com.khaled.tierandroidchallenge.common.TAG
import com.khaled.tierandroidchallenge.common.indefiniteSnackBar
import com.khaled.tierandroidchallenge.data.model.Attributes
import com.khaled.tierandroidchallenge.databinding.FragmentNearbyScootersMapBinding
import com.khaled.tierandroidchallenge.presentation.viewmodel.NearbyScootersViewModel
import com.khaled.tierandroidchallenge.util.CustomMapClusterRenderer
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NearbyScootersMapFragment :
    Fragment(),
    OnMapReadyCallback,
    ClusterManager.OnClusterItemClickListener<Attributes> {
    private var _binding: FragmentNearbyScootersMapBinding? = null
    private val binding get() = _binding!!
    private var googleMap: GoogleMap? = null
    private lateinit var clusterManager: ClusterManager<Attributes>
    private val viewModel: NearbyScootersViewModel by viewModels()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentNearbyScootersMapBinding.inflate(inflater, container, false)
        _binding?.setVariable(BR.vm, viewModel)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (googleMap == null) {
            setUpMap()
            observe()
        }
    }

    private fun setUpMap() {
        val mapFragment =
            childFragmentManager.findFragmentById(binding.map.id) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
    }

    private fun observe() {
        viewModel.event.observe(viewLifecycleOwner) {
            when (it) {
                is Resource.Failure -> {
                    binding.root.indefiniteSnackBar(it.error, functor = {
                        setAction(getString(R.string.retry_button)) { viewModel.retry() }
                    })
                    updateProgressBarVisibility(false)
                }
                is Resource.Loading -> {
                    updateProgressBarVisibility(true)
                }
                is Resource.Success -> {
                    val scooters = it.data
                    val attributes = scooters.data.map { scooter -> scooter.attributes }
                    addItemsToCluster(attributes = attributes)
                    zoomToScootersBound(attributes)
                    updateBannerTitle(scooters.data.size)
                    updateProgressBarVisibility(false)
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onMapReady(googleMap: GoogleMap) {
        this.googleMap = googleMap
        this.googleMap?.setOnMapLoadedCallback {
            viewModel.getNearbyScooters()
        }
        setUpClusterer()
    }

    private fun updateBannerTitle(size: Int) {
        viewModel.setVehiclesData(size)
    }

    private fun updateProgressBarVisibility(visible: Boolean) {
        viewModel.showProgressBar(visible)
    }

    private fun setUpClusterer() {
        clusterManager = ClusterManager(context, googleMap)
        clusterManager.renderer = CustomMapClusterRenderer(context, googleMap, clusterManager)
        clusterManager.setOnClusterItemClickListener(this)
        googleMap?.setOnCameraIdleListener(clusterManager)
    }

    private fun zoomToScootersBound(points: List<Attributes>) {
        val builder = LatLngBounds.Builder()
        points.onEach { builder.include(LatLng(it.lat, it.lng)) }
        adjustClusterZoom(builder.build())
    }

    private fun adjustClusterZoom(bounds: LatLngBounds) {
        val cu = CameraUpdateFactory.newLatLngBounds(bounds, 20)
        googleMap?.animateCamera(cu)
    }

    private fun addItemsToCluster(attributes: List<Attributes>) {
        for (attribute in attributes)
            clusterManager.addItem(attribute)
    }

    override fun onClusterItemClick(item: Attributes): Boolean {
        VehicleDetailsBottomSheet.newInstance(item).show(
            childFragmentManager,
            VehicleDetailsBottomSheet.TAG
        )
        return true
    }
}
