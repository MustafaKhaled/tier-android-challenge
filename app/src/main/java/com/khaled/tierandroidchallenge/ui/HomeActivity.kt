package com.khaled.tierandroidchallenge.ui

import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.activity.result.contract.ActivityResultContracts.RequestPermission
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import com.khaled.tierandroidchallenge.R
import com.khaled.tierandroidchallenge.ui.PermissionInfoDialog.OnDialogListener
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeActivity : AppCompatActivity(), OnDialogListener {
    private val requestPermissionLauncher =
        registerForActivityResult(
            RequestPermission()
        ) { isGranted: Boolean ->
            if (isGranted) {
                showNearbyScootersFragment()
            } else {
                showPermissionDialog()
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        checkLocationPermission()
    }

    private fun checkLocationPermission() {
        when {
            ContextCompat.checkSelfPermission(
                this,
                ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED -> {
                showNearbyScootersFragment()
            }
            shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION) -> {
                showPermissionDialog()
            }
            else -> {
                requestLocationPermission()
            }
        }
    }

    private fun showPermissionDialog() {
        showFragment(PermissionInfoDialog())
    }

    private fun showNearbyScootersFragment() {
        showFragment(NearbyScootersMapFragment())
    }

    private fun requestLocationPermission() {
        requestPermissionLauncher.launch(
            ACCESS_FINE_LOCATION
        )
    }

    private fun showFragment(fragment: Fragment) {
        supportFragmentManager.commit {
            setReorderingAllowed(true)
            replace(R.id.fragmentContainerView, fragment)
        }
    }

    override fun onAcceptPressed() {
        requestLocationPermission()
    }

    override fun onNoThanksPressed() {
        finish()
    }
}
