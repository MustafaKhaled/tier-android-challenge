package com.khaled.tierandroidchallenge.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.khaled.tierandroidchallenge.BR
import com.khaled.tierandroidchallenge.R
import com.khaled.tierandroidchallenge.common.withArguments
import com.khaled.tierandroidchallenge.data.model.Attributes
import com.khaled.tierandroidchallenge.databinding.LayoutScooterDetailsBinding
import com.khaled.tierandroidchallenge.presentation.viewmodel.VehicleDetailsViewModel
import com.khaled.tierandroidchallenge.presentation.viewmodel.VehicleDetailsViewModel.Event

class VehicleDetailsBottomSheet : BottomSheetDialogFragment() {
    private var _binding: LayoutScooterDetailsBinding? = null
    private val binding get() = _binding!!
    private val viewModel: VehicleDetailsViewModel by viewModels()
    companion object {
        private const val ARGS_ATTRIBUTES = "attributes"
        fun newInstance(attributes: Attributes) =
            VehicleDetailsBottomSheet().apply {
                withArguments(ARGS_ATTRIBUTES to attributes)
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.BottomSheetDialogStyle)
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = LayoutScooterDetailsBinding.inflate(inflater, container, false)
        _binding?.setVariable(BR.vm, viewModel)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindArguments()
        observe()
    }

    private fun observe() {
        viewModel.event.observe(viewLifecycleOwner) { state ->
            when (state) {
                Event.CloseBottomSheet -> dismiss()
            }
        }
    }

    private fun bindArguments() {
        val attribute = arguments?.get(ARGS_ATTRIBUTES) as Attributes
        viewModel.setData(attribute)
    }

    override fun onDetach() {
        super.onDetach()
        _binding = null
    }
}
