package com.khaled.tierandroidchallenge.di

import com.khaled.tierandroidchallenge.data.impl.impl.ScootersDataImpl
import com.khaled.tierandroidchallenge.domain.repo.IScootersData
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
interface IScooterDataModule {
    @Binds
    fun bindIScooterDataImpl(iscImpl: ScootersDataImpl): IScootersData
}
