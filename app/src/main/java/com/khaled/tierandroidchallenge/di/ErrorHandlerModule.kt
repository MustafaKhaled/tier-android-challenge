package com.khaled.tierandroidchallenge.di

import com.khaled.tierandroidchallenge.domain.errorhandling.ErrorHandler
import com.khaled.tierandroidchallenge.domain.errorhandling.NetworkErrorHandler
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
interface ErrorHandlerModule {
    @Binds
    fun bindNetworkErrorHandlerModule(networkErrorHandler: NetworkErrorHandler): ErrorHandler
}
