package com.khaled.tierandroidchallenge.di

import com.khaled.tierandroidchallenge.domain.errorhandling.NetworkErrorHandler
import com.khaled.tierandroidchallenge.domain.repo.IScootersData
import com.khaled.tierandroidchallenge.domain.usecases.ScooterCoordinatesUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
class UseCasesModule {
    @Provides
    fun provideScooterCoordinatesUseCase(
        iScootersData: IScootersData,
        networkErrorHandler: NetworkErrorHandler
    ): ScooterCoordinatesUseCase {
        return ScooterCoordinatesUseCase(iScootersData, networkErrorHandler)
    }
}
