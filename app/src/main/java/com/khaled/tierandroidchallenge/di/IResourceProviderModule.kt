package com.khaled.tierandroidchallenge.di

import com.khaled.tierandroidchallenge.common.IResourceProvider
import com.khaled.tierandroidchallenge.common.ResourceProvider
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface IResourceProviderModule {
    @Binds
    fun bindIResourceProvider(resourceProvider: ResourceProvider): IResourceProvider
}
