package com.khaled.tierandroidchallenge.domain.repo

import com.khaled.tierandroidchallenge.data.model.NearbyScooters

interface IScootersData {
    suspend fun getScooters(): NearbyScooters
}
