package com.khaled.tierandroidchallenge.domain.errorhandling

interface ErrorHandler {
    fun getError(throwable: Throwable): String
}
