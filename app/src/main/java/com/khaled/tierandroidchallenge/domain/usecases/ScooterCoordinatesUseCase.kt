package com.khaled.tierandroidchallenge.domain.usecases

import com.khaled.tierandroidchallenge.common.Resource
import com.khaled.tierandroidchallenge.domain.errorhandling.NetworkErrorHandler
import com.khaled.tierandroidchallenge.domain.repo.IScootersData
import kotlinx.coroutines.flow.flow
import java.lang.Exception
import javax.inject.Inject

class ScooterCoordinatesUseCase @Inject constructor(
    private val iScootersData: IScootersData,
    private val networkErrorHandler: NetworkErrorHandler
) {
    fun getNearbyScooters() = flow {
        emit(Resource.Loading)
        try {
            val result = iScootersData.getScooters()
            emit(Resource.Success(result))
        } catch (exception: Exception) {
            emit(Resource.Failure(networkErrorHandler.getError(exception)))
        }
    }
}
