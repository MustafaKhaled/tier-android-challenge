package com.khaled.tierandroidchallenge.domain.errorhandling

import com.khaled.tierandroidchallenge.R
import com.khaled.tierandroidchallenge.common.NetworkErrorEntity
import com.khaled.tierandroidchallenge.common.ResourceProvider
import retrofit2.HttpException
import java.net.HttpURLConnection
import javax.inject.Inject

class NetworkErrorHandler @Inject constructor(private val resourceProvider: ResourceProvider) :
    ErrorHandler {
    override fun getError(throwable: Throwable): String {
        return when (throwable) {
            is HttpException -> {
                when (throwable.code()) {

                    HttpURLConnection.HTTP_NOT_FOUND -> getErrorMessage(NetworkErrorEntity.NotFound)

                    HttpURLConnection.HTTP_FORBIDDEN -> getErrorMessage(NetworkErrorEntity.AccessDenied)

                    HttpURLConnection.HTTP_UNAVAILABLE or
                        HttpURLConnection.HTTP_INTERNAL_ERROR -> getErrorMessage(
                        NetworkErrorEntity.ServiceUnavailable
                    )

                    else -> getErrorMessage(NetworkErrorEntity.Unknown)
                }
            }
            else -> getErrorMessage(NetworkErrorEntity.Unknown)
        }
    }

    private fun getErrorMessage(errorEntity: NetworkErrorEntity): String {
        return when (errorEntity) {
            NetworkErrorEntity.AccessDenied -> resourceProvider.getString(R.string.cannot_access_resource_error_msg)
            NetworkErrorEntity.NotFound -> resourceProvider.getString(R.string.cannot_find_requested_data_msg)
            NetworkErrorEntity.ServiceUnavailable -> resourceProvider.getString(R.string.service_unavailable_msg)
            NetworkErrorEntity.Unknown -> resourceProvider.getString(R.string.general_error_msg)
        }
    }
}
