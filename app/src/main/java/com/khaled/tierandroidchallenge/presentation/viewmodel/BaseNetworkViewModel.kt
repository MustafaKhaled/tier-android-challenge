package com.khaled.tierandroidchallenge.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.khaled.tierandroidchallenge.common.Resource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch

abstract class BaseNetworkViewModel<T> : ViewModel() {

    lateinit var block: suspend () -> Flow<Resource<T>>
    private val _event = MutableLiveData<Resource<T>>()
    val event: LiveData<Resource<T>>
        get() = _event
    private val coroutineScope: CoroutineScope = CoroutineScope(Dispatchers.IO)
    suspend fun networkCall(
        block: suspend () -> Flow<Resource<T>>
    ) {
        viewModelScope.launch {
            sendNetworkRequest(block)
        }
    }

    private suspend fun sendNetworkRequest(block: suspend () -> Flow<Resource<T>>) {
        call(block)
    }

    private suspend fun call(block: suspend () -> Flow<Resource<T>>) {
        block.invoke().collect { state ->
            this.block = block
            when (state) {
                is Resource.Loading -> {
                    _event.postValue(Resource.Loading)
                }
                is Resource.Success -> {
                    _event.postValue(Resource.Success(state.data))
                }
                is Resource.Failure -> {
                    _event.postValue(Resource.Failure(state.error))
                }
            }
        }
    }

    fun retry() {
        coroutineScope.launch { call(block) }
    }

    override fun onCleared() {
        super.onCleared()
        coroutineScope.cancel()
    }
}
