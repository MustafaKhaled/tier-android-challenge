package com.khaled.tierandroidchallenge.presentation.viewmodel

import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.khaled.tierandroidchallenge.R
import com.khaled.tierandroidchallenge.common.exhaustive
import com.khaled.tierandroidchallenge.data.model.Attributes

class VehicleDetailsViewModel : ViewModel() {
    val data = ObservableField<Attributes>()
    val batteryLevelRes = ObservableInt()
    val event: LiveData<Event>
        get() = _event
    private val _event = MutableLiveData<Event>()
    fun setData(attributes: Attributes) {
        data.set(attributes)
        batteryLevelRes.set(getBatteryLevelRes(attributes.batteryLevel))
    }

    fun dismissDialog() {
        _event.value = Event.CloseBottomSheet
    }

    private fun getBatteryLevelRes(batteryLevel: Int): Int {
        return when (batteryLevel) {
            in 0..35 -> {
                R.drawable.ic_battery_low
            }
            in 36..75 -> {
                R.drawable.ic_battery_medium
            }
            in 75..100 -> {
                R.drawable.ic_battery_level_full
            }
            else -> {
                R.drawable.ic_baseline_battery_unknown_24
            }
        }.exhaustive
    }

    sealed class Event {
        object CloseBottomSheet : Event()
    }
}
