package com.khaled.tierandroidchallenge.presentation.viewmodel

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableInt
import androidx.lifecycle.viewModelScope
import com.khaled.tierandroidchallenge.data.model.NearbyScooters
import com.khaled.tierandroidchallenge.domain.usecases.ScooterCoordinatesUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NearbyScootersViewModel @Inject constructor(private val useCase: ScooterCoordinatesUseCase) :
    BaseNetworkViewModel<NearbyScooters>() {
    val showProgressBar = ObservableBoolean(false)
    val availableVehicle = ObservableInt(0)
    fun setVehiclesData(vehicles: Int) {
        availableVehicle.set(vehicles)
    }
    fun showProgressBar(visible: Boolean) {
        showProgressBar.set(visible)
    }
    fun getNearbyScooters() {
        viewModelScope.launch {
            networkCall { useCase.getNearbyScooters() }
        }
    }
}
