package com.khaled.tierandroidchallenge.util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.view.DefaultClusterRenderer
import com.khaled.tierandroidchallenge.R
import com.khaled.tierandroidchallenge.data.model.Attributes

class CustomMapClusterRenderer(
    val context: Context?,
    map: GoogleMap?,
    clusterManager: ClusterManager<Attributes>?
) : DefaultClusterRenderer<Attributes>(context, map, clusterManager) {
    override fun onBeforeClusterItemRendered(item: Attributes, markerOptions: MarkerOptions) {
        super.onBeforeClusterItemRendered(item, markerOptions)
        val properIconDrawable = getMarkerIcon(item.vehicleType)
        markerOptions.icon(
            BitmapDescriptorFactory.fromBitmap(
                Bitmap.createScaledBitmap(
                    BitmapFactory.decodeResource(
                        context?.resources,
                        properIconDrawable
                    ),
                    120, 120, false
                )
            )
        )
        item.icon = properIconDrawable
    }

    private fun getMarkerIcon(vehicleType: String): Int {
        return if (vehicleType == context?.getString(R.string.emoped_attribute))
            R.mipmap.ic_e_moped
        else
            R.mipmap.ic_e_scooter
    }
}
