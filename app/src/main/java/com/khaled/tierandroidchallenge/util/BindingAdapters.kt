package com.khaled.tierandroidchallenge.util

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.khaled.tierandroidchallenge.R

object BindingAdapters {
    @JvmStatic
    @BindingAdapter("android:visibility")
    fun setVisibility(view: View, visible: Boolean) {
        view.visibility = if (visible) View.VISIBLE else View.GONE
    }

    @JvmStatic
    @BindingAdapter("android:src")
    fun setImageViewSource(imageView: ImageView, id: Int) {
        imageView.setImageResource(id)
    }

    @JvmStatic
    @BindingAdapter("app:hasHelmet")
    fun setHasHelmetBox(textView: TextView, hasHelmet: Boolean) {
        if (hasHelmet)
            textView.text = textView.context.getString(R.string.has_helmet_yes)
        else
            textView.text = textView.context.getString(R.string.has_helmet_no)
    }
}
