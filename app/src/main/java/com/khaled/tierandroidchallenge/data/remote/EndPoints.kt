package com.khaled.tierandroidchallenge.data.remote

import com.khaled.tierandroidchallenge.data.model.NearbyScooters
import retrofit2.http.GET

interface EndPoints {
    @GET("public/take_home_test_data.json")
    suspend fun getNearbyScooters(): NearbyScooters
}
