package com.khaled.tierandroidchallenge.data.impl.impl

import com.khaled.tierandroidchallenge.data.model.NearbyScooters
import com.khaled.tierandroidchallenge.data.remote.EndPoints
import com.khaled.tierandroidchallenge.domain.repo.IScootersData
import javax.inject.Inject

class ScootersDataImpl @Inject constructor(private val endPoints: EndPoints) : IScootersData {
    override suspend fun getScooters(): NearbyScooters {
        return endPoints.getNearbyScooters()
    }
}
