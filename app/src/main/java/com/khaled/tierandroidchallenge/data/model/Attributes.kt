package com.khaled.tierandroidchallenge.data.model

import android.os.Parcelable
import com.google.android.gms.maps.model.LatLng
import com.google.gson.annotations.SerializedName
import com.google.maps.android.clustering.ClusterItem
import kotlinx.android.parcel.Parcelize

@Parcelize
class Attributes(
    @SerializedName("batteryLevel") val batteryLevel: Int,
    @SerializedName("lat") val lat: Double,
    @SerializedName("lng") val lng: Double,
    @SerializedName("maxSpeed") val maxSpeed: Int,
    @SerializedName("vehicleType") val vehicleType: String,
    @SerializedName("hasHelmetBox") val hasHelmetBox: Boolean,
    var icon: Int
) : ClusterItem, Parcelable {

    override fun getPosition(): LatLng = LatLng(lat, lng)

    override fun getTitle(): String = ""

    override fun getSnippet(): String = ""
}
