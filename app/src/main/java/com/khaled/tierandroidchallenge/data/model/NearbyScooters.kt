package com.khaled.tierandroidchallenge.data.model

import com.google.gson.annotations.SerializedName

data class NearbyScooters(
    @SerializedName("data") val data: ArrayList<Scooter>
)
