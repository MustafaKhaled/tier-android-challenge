package com.khaled.tierandroidchallenge.data.model

import com.google.gson.annotations.SerializedName

data class Scooter(
    @SerializedName("type") val type: String,
    @SerializedName("id") val id: String,
    @SerializedName("attributes") val attributes: Attributes
)
