package com.khaled.tierandroidchallenge.common

sealed class NetworkErrorEntity {

    object NotFound : NetworkErrorEntity()

    object AccessDenied : NetworkErrorEntity()

    object ServiceUnavailable : NetworkErrorEntity()

    object Unknown : NetworkErrorEntity()
}
