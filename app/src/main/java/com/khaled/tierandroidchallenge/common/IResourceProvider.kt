package com.khaled.tierandroidchallenge.common

import androidx.annotation.StringRes

interface IResourceProvider {
    fun getString(@StringRes resId: Int): String
}
