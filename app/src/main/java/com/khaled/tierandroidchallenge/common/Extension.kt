package com.khaled.tierandroidchallenge.common

import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar

val Any.TAG: String
    get() {
        return javaClass.simpleName
    }

inline fun <reified T : Fragment> T.withArguments(vararg params: Pair<String, Any>): T {
    arguments = bundleOf(*params)
    return this
}

val <T> T.exhaustive: T
    get() = this

inline fun View.indefiniteSnackBar(
    text: String,
    duration: Int = Snackbar.LENGTH_INDEFINITE,
    functor: Snackbar.() -> Unit
) {
    val snackbar: Snackbar = Snackbar.make(this, text, duration)
    snackbar.functor()
    snackbar.show()
}
