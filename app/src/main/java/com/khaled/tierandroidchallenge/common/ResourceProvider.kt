package com.khaled.tierandroidchallenge.common

import android.content.Context
import javax.inject.Inject

class ResourceProvider @Inject constructor(val context: Context) : IResourceProvider {
    override fun getString(resId: Int) = context.resources.getString(resId)
}
